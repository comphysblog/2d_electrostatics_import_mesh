// Gmsh project created on Tue Aug 14 22:02:37 2018
SetFactory("OpenCASCADE");
//+
Disk(1) = {0, 0, 0, 0.5, 0.5};
//+
Disk(2) = {0, 0, 0, 3, 3};
//+
BooleanDifference{ Surface{2}; Delete; }{ Surface{1}; Delete; }
//+
Physical Surface("vacuum", 1) = {2};
//+
Physical Line("inner", 2) = {1};
//+
Physical Line("outer", 3) = {2};
//+
Transfinite Line {1} = 30 Using Progression 1;
